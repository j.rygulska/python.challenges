import random

capital_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Daleware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

state, capital = random.choice(list(capital_dict.items()))
while True:
    user = input(f"What is the capital of {state}? ").lower()
    if user == capital.lower():
            print("Correct")
            break
    elif user == "Exit".lower():
            print(f"Capital of {state} is {capital}. Goodbye")
            break
