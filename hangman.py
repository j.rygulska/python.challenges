import random 

tries = [
"""
  ___
 |   |
 |
 |
 |
/ \\
""",
"""
  ___
 |   |
 |   0
 |
 |
/ \\
 """,
 """
   ___
  |   |
  |   0
  |   |
  |       
 / \\
 """,
 """
   ___
  |   |
  |   0
  |  /|
  |       
 / \\
 """,
 """
   ___
  |   |
  |   0
  |  /|\\
  |       
 / \\
 """,
 """
   ___
  |   |
  |   0
  |  /|\\
  |  /    
 / \\
 """,
 """
   ___
  |   |
  |   0
  |  /|\\
  |  / \\   
 / \\
 """]

words_list = ["flower", "dictionary", "elephant", "summer", "winter",
              "autumn", "spring", "mushroom", "bookshelf", "clothes",
              "banana", "pineapple", "watermelon", "sandwich", "dishwasher",
              "cooker", "country", "capital", "snowflake", "snowman", "cartoon",
              "giraffe", "pattern"]

used_letters = []

def get_letter():
    letter =(input("\nEnter your letter: ")).upper()
    return letter


def check_letter(letter):
    """
    returns True if user chose a correct letter
    """
    correct_letter = True
    global used_letters
    if letter in used_letters:
        correct_letter = False
        print("You have already used that letter.")
    else:
        used_letters.append(letter)
    print(f"Used letters:{','.join(used_letters)}")
    return correct_letter

def print_letter_list(letter_list):
    print (f"{' '.join(letter_list)}")

 
print("Welcome in a 'Hangman' play. You have chance to guess the word. Let's start the game!")
print("*" * 60)
print (f"You have {len(tries)} chances to guess the word")

word = random.choice(words_list).upper()

guessed_word = ["_"] * len(word)
print (f"Word to guess has {len(word)} letters.")
print_letter_list(guessed_word)

level = 0
while level < len(tries):
    letter = get_letter()
    if check_letter(letter) == False:
        print_letter_list(guessed_word)
        continue
    
    if letter in word:
        print ("It's correct letter")
        for index in range(len(word)):
            if letter == word[index]:
                guessed_word[index] = letter
        print_letter_list(guessed_word)
    else:
        print ("It's incorrect letter")
        print(tries[level])
        print_letter_list(guessed_word)
        level += 1
    
    if level == len(tries):
        print("You loose!")
        print(f"Guessed word was: {word}")
        break
        
    if ''.join(guessed_word) == word:
        print("That's the word! You won!")
        break

