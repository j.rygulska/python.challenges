import random

def coin_flip():
    """Randomly return 'heads' or 'tails'."""
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

flips = 0

for trial in range(10_000):
    flips = flips + 1
    if coin_flip() == "heads":
        while coin_flip() == "heads":
            flips = flips + 1

    else:
        while coin_flip() == "tails":
            flips = flips + 1

    flips = flips + 1
 
average = flips / 10_000
print(f"The average number of flips per trial is {average}.")
