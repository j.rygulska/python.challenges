m = 100
k = 100
def num_dividers():
    dividers = [0] * 100
    global m
    global k
    for i in range(1, m + 1):
        for n in range(1, k + 1):
            if i % n == 0:
                dividers[i -1] += 1 

    return dividers

cats = num_dividers()

for i, result in enumerate(cats, 1):
    if result % 2 == 0:
        print(f"Cat {i} has not a hat.")
    else:
        print(f"Cat {i} has a hat.")
