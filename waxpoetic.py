import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounced", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

def poetry():

    adj1 = random.choice(adjectives)
    noun1 = random.choice(nouns)
    verb1 = random.choice(verbs)
    prep1 = random.choice(prepositions)
    adverb1 = random.choice(adverbs)
    adj2 = random.choice(adjectives)
    noun2 = random.choice(nouns)
    verb2 = random.choice(verbs)
    prep2 = random.choice(prepositions)
    adj3 = random.choice(adjectives)
    noun3 = random.choice(nouns)
    verb3 = random.choice(verbs)

    while adj1 == adj2:
        adj2 = random.choice(adjectives)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjectives)

    while noun1 == noun2:
        noun2 = random.choice(nouns)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(nouns)

    while verb1 == verb2:
        verb2 = random.choice(verbs)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(verbs)

    while prep1 == prep2:
        prep2 = random.choice(prepositions)


    if "aeiou".find(adj1[0]) != -1:
        first = "An"
    else:
        first = "A"
        

    if "aeiou".find(adj3[0]) != -1:
          second = "an"
    else:
        second = "a"

    poem = print(f"{first} {adj1} {noun1}"
            '\n' '\n'
            f"{first} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}"
            '\n'
            f"{adverb1}, the {noun1} {verb2}"
            '\n'
            f"the {noun2} {verb3} {prep2} {second} {adj3} {noun3}")

    return poem


poem = poetry()
print(poem)
