def enrollment_stats(universities):
    total_students = []
    total_tuition = []

    for university in universities:
        total_students.append(university[1])
        total_tuition.append(university[2])

    return total_students, total_tuition

def mean(sth):
    return sum(sth)/len(sth)

def median(sth):
    sth.sort()
    if len(sth) % 2 == 1:
        result1 = int(len(sth) / 2)
        return sth[result1]
    else:
        left = int(len(sth) - 1) / 2
        right = int(len(sth) + 1) / 2
        return mean(sth[left], sth[right])
        
    
  
universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachussetts Institute od Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

total = enrollment_stats(universities)

print("*" * 30)
print(f"Total students: {sum(total[0]):,}")
print(f"Total tuition: $ {sum(total[1]):,}")
print("\n")
print(f"Student mean: {mean(total[0]):,.2f}")
print(f"Student median: {median(total[0]):,}")
print("\n")
print(f"Tuition mean: $ {mean(total[1]):,.2f}")
print(f"Tuition median: $ {median(total[1]):,}")
print("*" * 30)
