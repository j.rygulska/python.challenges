import random

all_votes_a = 0
all_votes_b = 0

for votes in range(10_000):
    candidate_a = 0
    candidate_b = 0
    
    if random.random() < 0.87:
        candidate_a = candidate_a + 1
    else:
        candidate_b = candidate_b + 1
        

    if random.random() < 0.65:
        candidate_a = candidate_a + 1
    else:
        candidate_b = candidate_b + 1
        

    if random.random() < 0.17:
        candidate_a = candidate_a + 1
    else:
        candidate_b = candidate_b + 1
        

    if candidate_a > candidate_b:
        all_votes_a = all_votes_a + 1
    else:
        all_votes_b = all_votes_b + 1




result_a = all_votes_a/10_000
print(f"Probability that Candidate A wins is {result_a}.")

result_b = all_votes_b/10_000
print(f"Probability that Candidate B wins is {result_b}.")
