class Animal:
    def __init__(self, name, age, hungry_level):
        self.name = name
        self.age = age
        self.level_food = 0
        self.hungry_level = hungry_level

    def feed(self, grams_food):
        self.level_food += grams_food
    
    def speak(self, sound):
        return f"{self.name} says {sound}"
      
    def get_food_level(self):
        if self.level_food < self.hungry_level:
          is_hungry = "Animal is hungry."
        else:
          is_hungry = "Animal is not hungry."
        return f"Level food in {self.name}'s belly is: {self.level_food}. {is_hungry}"

class Dog(Animal):
    
    def __init__(self, name, age):
        super().__init__(name, age, 100)
 
    def __str__(self):
        return super().speak("Woof!")
 
class Cat(Animal):
    
    def __init__(self, name, age):
        super().__init__(name, age, 50)
 
    def __str__(self):
        return super().speak("Meow!")
 
class Horse(Animal):
    
    def __init__(self, name, age):
        super().__init__(name, age, 200)
 
    def __str__(self):
        return super().speak("Neigh")
 
 
bob = Dog("Bob", 4)
jack = Cat("Jack", 7)
leo = Cat("Leo", 2)
katty = Horse("Katty", 3)
jim = Dog("Jim", 6)
 
all_animals = [bob, jack, leo, katty, jim]

jack.feed(50)
jim.feed(120)
bob.feed(30)
leo.feed(30)
katty.feed(280)

for animal in all_animals:
    print(animal)
    print(animal.get_food_level())