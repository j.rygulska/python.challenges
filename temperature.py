def convert_cel_to_far(c):
    """Return the Celsius temperature converted to Fahrenheit."""
    return c * 9/5 + 32

def convert_far_to_cel(f):
    """Return the Fahrenheit temperature converted to Celsius."""
    return (f - 32) * 5/9 

c = input("Enter a temperature in degrees C: ")
f = convert_cel_to_far(float(c))    
print(f"{c} degrees C = {f:.2f} degrees F")

f = input("Enter a temperature in degrees F: ")
c = convert_far_to_cel(float(f))    
print(f"{f} degrees F = {c:.2f} degrees C")
